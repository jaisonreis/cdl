module.exports = (grunt) ->

  grunt.initConfig
    pkg: grunt.file.readJSON 'package.json'
    compass:
      options:
        config: 'config.rb'

      watch:
        options:
          watch: true

    concat:
      dist:
        src: [
          'bower_components/foundation/js/vendor/jquery.js'
          'bower_components/jquery-ui/ui/jquery-ui.js'
          'bower_components/foundation/js/vendor/fastclick.js'
          'bower_components/foundation/js/vendor/jquery.cookie.js'
          'bower_components/foundation/js/vendor/placeholder.js'
          'bower_components/foundation/js/foundation.min.js'
          'bower_components/angular/angular.min.js'
          'bower_components/angular-ui-utils/ui-utils.js'
          'bower_components/angular-ui-date/src/date.js'
          'js/cardcheck.min.js'
        ]
        dest: 'js/vendor.js'

    coffee:
      dist:
        expand: true
        cwd: 'coffee'
        src: [ '**/*.coffee' ]
        dest: 'js'
        ext: '.js'

    jade:
      dist:
        options:
          pretty: true
          data:
            debug: true
        files:
          "index.html": 'index.jade'
          "listagem.html": 'listagem.jade'
          "login.html": 'login.jade'
          "cadastro.html": 'cadastro.jade'
          "carrinho.html": 'carrinho.jade'
          "checkout.html": 'checkout.jade'
          "confirmacao.html": 'confirmacao.jade'
          "meus-dados.html": 'meus-dados.jade'
          "meus-pedidos.html": 'meus-pedidos.jade'
          "pedido.html": 'pedido.jade'
          "tenha-sua-loja.html": 'tenha-sua-loja.jade'
          "sobre.html": 'sobre.jade'

    php:
      dist:
        options:
          port: 3000
          keepalive: true
          open: true

    watch:
      grunt:
        files: [ 'Gruntfile.coffee' ]

      jade:
        files: [ '**/*.jade' ]
        tasks: [ 'jade' ]

      coffee:
        files: [ 'coffee/**/*.coffee' ]
        tasks: [ 'coffee' ]

    concurrent:
      watch:
        options:
          logConcurrentOutput: true
          limit: 3
        tasks: [
          'compass:watch'
          'watch'
          'php'
        ]

  grunt.loadNpmTasks 'grunt-contrib-compass'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-contrib-concat'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-jade'
  grunt.loadNpmTasks 'grunt-php'
  grunt.loadNpmTasks 'grunt-concurrent'


  grunt.registerTask 'build', [
    'concat'
    'jade'
    'coffee'
    'compass:dist'
  ]

  grunt.registerTask 'default', [
    'concat'
    'jade'
    'coffee'
    'concurrent:watch'
  ]
