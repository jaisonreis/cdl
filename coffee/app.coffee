'use strict'

$(document).ready ->
  $(document).foundation()

  $('#credit-card').cardcheck
    iconLocation: "#accepted-cards-images"
    iconDir: "img/"

  $('#search-small-btn').click ->
    if $(@).parents('.top-bar').hasClass 'expanded'
      $(@).parents('.top-bar').find('.toggle-topbar').click()
    $('#profile-small').hide()
    $('#search-small').toggle()


  $('#profile-small-btn').click ->
    if $(@).parents('.top-bar').hasClass 'expanded'
      $(@).parents('.top-bar').find('.toggle-topbar').click()
    $('#search-small').hide()
    $('#profile-small').toggle()

  $('.toggle-topbar').click ->
    $('#search-small').hide()
    $('#profile-small').hide()

  $('.city-selector .selected').click ->
    $(@).parent().find('.options').toggle()

  $('.city-selector .option').click ->
    f = $(@).parents('.city-selector')
    s = f.find('.selected')
    s.text $(@).text()
    f.find('.options').hide()


  $('[data-orbit]').on 'replace', 'img', (e, new_path, original_path) ->
    $('[data-orbit]').foundation('orbit').resize()


  $('.tab-select-loja a').click (e) ->
    $('.store-tab').hide()
    $($(@).attr('href')).show()
    $(@).parents('ul').find('li').removeClass 'active'
    $(@).parent('li').addClass 'active'

    e.preventDefault()
    e.stopPropagation()
    return false

  $('.lojas-destaque').each ->
    $(@).find('.store-tab').hide();
    $(@).find('.store-tab:first').show();

  $('.filtro-tree > ul > li > a').click (e) ->
    tree = $(@).parents('ul')
    li = $(@).parent('li')

    tree.children('li').removeClass('open')
    li.addClass('open')

    e.preventDefault()
    e.stopPropagation()
    return false





