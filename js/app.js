(function() {
  'use strict';
  $(document).ready(function() {
    $(document).foundation();
    $('#credit-card').cardcheck({
      iconLocation: "#accepted-cards-images",
      iconDir: "img/"
    });
    $('#search-small-btn').click(function() {
      if ($(this).parents('.top-bar').hasClass('expanded')) {
        $(this).parents('.top-bar').find('.toggle-topbar').click();
      }
      $('#profile-small').hide();
      return $('#search-small').toggle();
    });
    $('#profile-small-btn').click(function() {
      if ($(this).parents('.top-bar').hasClass('expanded')) {
        $(this).parents('.top-bar').find('.toggle-topbar').click();
      }
      $('#search-small').hide();
      return $('#profile-small').toggle();
    });
    $('.toggle-topbar').click(function() {
      $('#search-small').hide();
      return $('#profile-small').hide();
    });
    $('.city-selector .selected').click(function() {
      return $(this).parent().find('.options').toggle();
    });
    $('.city-selector .option').click(function() {
      var f, s;
      f = $(this).parents('.city-selector');
      s = f.find('.selected');
      s.text($(this).text());
      return f.find('.options').hide();
    });
    $('[data-orbit]').on('replace', 'img', function(e, new_path, original_path) {
      return $('[data-orbit]').foundation('orbit').resize();
    });
    $('.tab-select-loja a').click(function(e) {
      $('.store-tab').hide();
      $($(this).attr('href')).show();
      $(this).parents('ul').find('li').removeClass('active');
      $(this).parent('li').addClass('active');
      e.preventDefault();
      e.stopPropagation();
      return false;
    });
    $('.lojas-destaque').each(function() {
      $(this).find('.store-tab').hide();
      return $(this).find('.store-tab:first').show();
    });
    return $('.filtro-tree > ul > li > a').click(function(e) {
      var li, tree;
      tree = $(this).parents('ul');
      li = $(this).parent('li');
      tree.children('li').removeClass('open');
      li.addClass('open');
      e.preventDefault();
      e.stopPropagation();
      return false;
    });
  });

}).call(this);
